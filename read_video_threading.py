import cv2
import threading
from enum import Enum
import time


# -------------------Begin CamReConnect-------------------
class EventCase(Enum):
    Run = 0
    Pause = 1
    Stop = 2
    Reopen = 3
    Quit = 4
    Nothing = 5


class CamReader:

    def __init__(self, num_cam, cam_index, filename, num_framedrop, frame_buffer, no_job_sleep_time, event_queue,
                 wait_stop, ref_commands, status_list):
        self.num_cam = num_cam
        self.cam_index = cam_index
        self.filename = filename
        self.num_framedrop = num_framedrop
        self.frame_buffer = frame_buffer
        self.no_job_sleep_time = no_job_sleep_time
        self.event_queue = event_queue
        self.wait_stop = wait_stop
        self.ref_commands = ref_commands
        self.status_list = status_list
        self.cap = None
        self.status = EventCase.Reopen
        self.frame_index = -1
        self.open_by_thread()
        self.run_by_threading()
        self.grab_failed_count = 0

    # ------------------------------------------------------------------------------------
    def open(self, first_sleep_time):

        if first_sleep_time != 0.0:
            time.sleep(first_sleep_time)

        while True:
            cap = cv2.VideoCapture(self.filename)
            print("[INFO]-- VideoCapture -> ", cap)
            if cap is not None:
                self.w = int(cap.get(3))
                self.h = int(cap.get(4))
                self.ori_fps = cap.get(cv2.CAP_PROP_FPS)
                if self.ori_fps > 240:
                    self.ori_fps = 240

                if self.ori_fps > 0:
                    self.no_job_sleep_time = 1.0 / self.ori_fps
                else:
                    self.no_job_sleep_time = 1.0 / 30

                print("[INFO]-- Cam ", self.cam_index, " ori_fps: ", self.ori_fps, " no_job_sleep_time: ",
                      self.no_job_sleep_time)

                self.frame_len = cap.get(cv2.CAP_PROP_FRAME_COUNT)
                self.is_camera = (self.filename.find("://") != -1)
                self.cap = cap
                self.status = EventCase.Run
                self.status_list[self.cam_index] = True
                self.grab_failed_count = 0
                self.div_keep = -1
                break

            if first_sleep_time < 1:
                first_sleep_time = 1
            elif first_sleep_time < 60:
                first_sleep_time += 1

            print("[INFO]-- Sleeping time in open cam: ", first_sleep_time)
            time.sleep(first_sleep_time)

        print("[INFO]-- Cam index {}, Open Successfully.".format(self.cam_index))

    def open_by_thread(self, first_sleep_time=0):
        th = threading.Thread(target=self.open, args=[first_sleep_time])
        th.start()

    # ------------------------------------------------------------------------------------
    def run(self):

        print("[INFO]-- Cam index {} is running".format(self.cam_index))
        count = 0
        sum_sleep_time = 0
        pausing = False

        ori_time = time.time()
        int_second_duration = 0
        num_drop = 0
        put_queue = 0

        while True:
            begin_time = time.time()
            count += 1

            # event parser, only for first threading (have cam_index = 0)
            if (self.cam_index == 0) and (not self.event_queue.empty()):
                command = self.event_queue.get()
                print("[INFO]-- Event parser, got command: ", command)

                if command == "stop":
                    for i in range(self.num_cam):
                        self.ref_commands[i] = EventCase.Quit
                elif command == "pause/unpause":
                    pausing = not pausing
                    for i in range(self.num_cam):
                        self.ref_commands[i] = EventCase.Pause if pausing else EventCase.Run
                elif "stop-cam:" in command:
                    stop_cam_index = int(command.split(":")[1])
                    if (stop_cam_index >= 0) and (stop_cam_index < self.num_cam):
                        self.ref_commands[stop_cam_index] = EventCase.Stop
                elif "reopen-cam:" in command:
                    stop_cam_index = int(command.split(":")[1])
                    if (stop_cam_index >= 0) and (stop_cam_index < self.num_cam):
                        self.ref_commands[stop_cam_index] = EventCase.Reopen
                elif "reopen-all" in command:
                    for i in range(self.num_cam):
                        self.ref_commands[i] = EventCase.Reopen

            # event processing
            if self.ref_commands[self.cam_index] == EventCase.Quit:
                self.ref_commands[self.cam_index] = EventCase.Nothing
                self.status = EventCase.Quit
                if self.frame_buffer.full():
                    self.frame_buffer.get()

                self.frame_buffer.put([-1, None])

                break

            elif self.ref_commands[self.cam_index] == EventCase.Pause:
                pausing = True
                self.status = EventCase.Pause
                self.ref_commands[self.cam_index] = EventCase.Nothing

            elif self.ref_commands[self.cam_index] == EventCase.Run:
                pausing = False
                self.status = EventCase.Run
                self.status_list[self.cam_index] = True
                self.ref_commands[self.cam_index] = EventCase.Nothing

            elif self.ref_commands[self.cam_index] == EventCase.Stop:
                if self.cap is not None:
                    self.cap.release()
                    self.cap = None
                self.status = EventCase.Stop
                self.status_list[self.cam_index] = False
                self.ref_commands[self.cam_index] = EventCase.Nothing

            elif self.ref_commands[self.cam_index] == EventCase.Reopen:
                if self.cap is not None:
                    self.cap.release()
                    self.cap = None

                self.status = EventCase.Reopen
                self.open_by_thread()
                self.ref_commands[self.cam_index] = EventCase.Nothing

            if pausing:
                time.sleep(self.no_job_sleep_time)
                continue

            if (self.status == EventCase.Run) and (self.cap is not None):

                ret = self.cap.grab()

                if ret:
                    self.frame_index += 1
                    div = (self.frame_index // self.num_framedrop)
                    if div != self.div_keep:  # not drop
                        self.div_keep = div
                        ret, frame = self.cap.retrieve()
                        if ret and (not self.frame_buffer.full()):
                            self.frame_buffer.put([self.frame_index, frame])
                            put_queue += 1

                        elif ret:
                            num_drop += 1
                            # if num_drop % 100 == 0:
                            #     print("Cam {} drop frame: {}".format(self.cam_index, num_drop))
                        # print("Cam {} put frame: {}".format(self.cam_index, put_queue))
                else:
                    self.grab_failed_count += 1

                    if (self.grab_failed_count > self.ori_fps * 5):

                        if self.is_camera:
                            self.status = EventCase.Stop
                            self.status_list[self.cam_index] = False
                            self.cap.release()
                            self.status = EventCase.Reopen
                            self.open_by_thread(1)
                        else:
                            if self.frame_buffer.full():
                                self.frame_buffer.get()
                            self.frame_buffer.put([-1, None])
                            break

            end_time = time.time()
            duration = end_time - begin_time
            if (self.no_job_sleep_time - duration) > 0.010:
                sleep_time = (self.no_job_sleep_time - duration) - 0.002
                sum_sleep_time += sleep_time
                time.sleep(sleep_time)
            else:
                time.sleep(0.001)

            if int(int_second_duration / 30) < int((end_time - ori_time) / 30):  # 30 second display 1 time
                int_second_duration = int(end_time - ori_time)
                sleep_time_percent = round(sum_sleep_time / (end_time - ori_time) * 100, 3)
                # print("[INFO]-- Cam index {}, Ave of sleeping time in percent: {}%".format(self.cam_index,
                #                                                                           sleep_time_percent))

        # before stop
        if self.cap is not None:
            self.cap.release()

        if self.cam_index == 0:
            print("[INFO]-- Read video threading is waiting to stop")
            self.wait_stop.wait()
            print("(1)--- Stopped Read video threading")
        # else:
        #     print("[INFO]-- Cam {} stopped".format(self.cam_index))

    def run_by_threading(self):
        th = threading.Thread(target=self.run, args=[])
        th.start()


def read_video(list_filename, list_num_framedrop, list_frame_buffer, no_job_sleep_time, event_queue, wait_stop,
               status_list):
    """
        full_delay_time is num of second to set sleep time
    """

    print("(1)--- Running Read video threading")
    num_cam = len(list_filename)
    ref_commands = [EventCase.Run] * num_cam

    # total number of cameras
    # print("#" * 30, " num_cam : ", num_cam)

    for i in range(num_cam):
        cam_reader = CamReader(num_cam, i, list_filename[i], list_num_framedrop[i], list_frame_buffer[i], 1.0 / 30,
                               event_queue, wait_stop, ref_commands, status_list)


def read_video_by_threading(list_filename, list_num_framedrop, list_frame_buffer, no_job_sleep_time, event_queue,
                            wait_stop, status_list):
    """
        full_delay_time is num of second to set sleep time
    """

    t = threading.Thread(target=read_video, args=[list_filename, list_num_framedrop, list_frame_buffer,
                                                  no_job_sleep_time, event_queue, wait_stop, status_list])
    t.start()
