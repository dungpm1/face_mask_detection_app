import sqlite3
import os
import yaml
import json


def final_create_db(dbname):
    if os.path.exists("./database/final_data_base.db"):
        os.remove("./database/final_data_base.db")
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    # Create table - DATA - with 6 features
    c.execute('''CREATE TABLE DATA
                     ([object_id] text,
                     [camera_name] text,
                     [camera_id] text,
                     [num_in] integer,
                     [num_mask] integer,
                     [num_no_mask] integer,
                     [minute] integer, 
                     [hour] integer, 
                     [day] integer, 
                     [month] integer, 
                     [year] integer)''')
    conn.commit()


def clear_config_json_file():
    config_file = "./configs/cameras_config.yml"
    # load dat in config file
    yaml.warnings({'YAMLLoadWarning': False})
    with open(config_file, 'r') as fs:
        config = yaml.load(fs)
    cam_config_first_time = config["input"]["cam_config"]
    with open(cam_config_first_time) as json_file:
        json_data = json.load(json_file)
    json_file.close()
    json_data["object_id"] = ""
    json_data["data"] = []
    # write json file
    with open(cam_config_first_time, "w") as outfile_edit:
        json.dump(json_data, outfile_edit)
    outfile_edit.close()


if __name__ == "__main__":
    # create blank database
    data_base_name = "./database/final_data_base.db"
    final_create_db(data_base_name)
    # clear configs file
    clear_config_json_file()
