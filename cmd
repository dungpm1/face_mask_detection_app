Installation
	pip3 install --user pyqt5  
	sudo apt-get install python3-pyqt5  
	sudo apt-get install pyqt5-dev-tools
	sudo apt-get install qttools5-dev-tools

Run from terminal
	qtchooser -run-tool=designer -qt=5

Conver to .py file from .ui file
	pyuic5 -x <.ui file name> -o <.py file name>
	
Install packages using requirements file
	pip install -r requirements.txt
	
Build APP using Pyinstaller
	pyinstaller --distpath ./GG_Face_Mask_APP_GPU/Dist/ --workpath ./GG_Face_Mask_APP_GPU/Build/ --specpath ./GG_Face_Mask_APP_GPU/ GG-Face-Mask-Application.py
	
	pyinstaller --distpath ./GG_Face_Mask_APP/Dist/ --workpath ./GG_Face_Mask_APP/Build/ --specpath ./GG_Face_Mask_APP/ GG-Face-Mask-Application.py
	
Install torch and torchvision for CPU computer only
	pip install torch==1.7.0+cpu torchvision==0.8.1+cpu torchaudio===0.7.0 -f https://download.pytorch.org/whl/torch_stable.html
