import yaml
import json
import os


def get_password(password_file):
    with open(password_file) as json_file:
        pass_data = json.load(json_file)
    json_file.close()
    password = pass_data["password"]
    return password


def configuration_file_infor(configuration_file):
    # load host and port in configuration file
    with open(configuration_file) as json_configuration_file:
        json_configuration_data = json.load(json_configuration_file)
    json_configuration_file.close()
    return json_configuration_data


def read_config_file(config_file):
    # load dat in config file
    yaml.warnings({'YAMLLoadWarning': False})
    with open(config_file, 'r') as fs:
        config = yaml.load(fs)
    cam_config_first_time = config["input"]["cam_config"]
    with open(cam_config_first_time) as json_file:
        json_data = json.load(json_file)
    json_file.close()
    return json_data


def update_grid(config_file):
    number_of_cameras_data = read_config_file(config_file)
    number_of_cameras = len(number_of_cameras_data["data"])
    grid_column = 1
    grid_row = 1
    # set suitable number of columns and rows based on number of cameras
    if number_of_cameras == 2:
        grid_column = 2
        grid_row = 1
    elif number_of_cameras == 3 or number_of_cameras == 4:
        grid_column = 2
        grid_row = 2
    elif number_of_cameras == 5 or number_of_cameras == 6:
        grid_column = 3
        grid_row = 2
    elif 7 <= number_of_cameras <= 9:
        grid_column = 3
        grid_row = 3
    elif 10 <= number_of_cameras <= 12:
        grid_column = 4
        grid_row = 3
    elif 13 <= number_of_cameras <= 16:
        grid_column = 4
        grid_row = 4
    elif 17 <= number_of_cameras <= 20:
        grid_column = 5
        grid_row = 5
    # update grid
    yaml.warnings({'YAMLLoadWarning': False})
    with open(config_file, 'r') as fs:
        config = yaml.load(fs)
    config["view_window"]["grid_col"] = grid_column
    config["view_window"]["grid_row"] = grid_row
    with open(config_file, 'w') as file:
        yaml.dump(config, file)
    file.close()


def automatically_delete_draw_figures():
    path1 = './draw/draw_counting_image.jpg'
    path2 = './draw/draw_region_image.jpg'
    path3 = './draw/original_image.jpg'
    if os.path.isfile(path1):
        os.remove(path1)
    if os.path.isfile(path2):
        os.remove(path2)
    if os.path.isfile(path3):
        os.remove(path3)
