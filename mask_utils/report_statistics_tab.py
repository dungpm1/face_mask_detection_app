import sqlite3
import matplotlib.pyplot as plt
import pandas as pd
import os
import time
from mask_utils import app_warning_function
import math
from matplotlib.ticker import MaxNLocator


def plot_save_export(name_of_figure,
                     color,
                     camera_name_input,
                     statistics_type,
                     date,
                     save_figure_flag,
                     export_data_flag):
    # connect to sql database
    conn = sqlite3.connect('./database/final_data_base.db')
    c = conn.cursor()
    return_data_final = []
    # return_data_final_all_cameras = []
    sum_nguoi = 0
    sum_co_KT = 0
    sum_khong_KT = 0

    # get camera_id of camera_name_input
    query_id = f"SELECT camera_id FROM DATA WHERE camera_name = '{camera_name_input}'"
    c.execute(query_id)
    # id_data = c.fetchall()
    # if len(c.fetchall()) > 0:
    #     print(c.fetchall())
    id_data = c.fetchall()

    if len(id_data) == 0:
        app_warning_function.query_camera_id_warning()
    else:
        camera_id = id_data[0][0]
        # print("camera_id: ", camera_id)

        if statistics_type == "Thống kê theo Ngày":
            year_input = date[-4:]
            day_input = date[0:2]
            month_input = date[3:5]

            query = f"SELECT camera_name,num_in,num_mask,num_no_mask,minute,hour,day,month,year " \
                    f"FROM DATA WHERE camera_id = '{camera_id}' and year = {year_input} and day = {day_input} " \
                    f"and month = {month_input} "
            c.execute(query)
            return_data = c.fetchall()
            # combine data and calculate percent
            ex_return_data_final = [list(ele) for ele in return_data]
            for i in range(len(ex_return_data_final)):
                ex_return_data_final[i][4] = "x"
                if len(return_data_final) == 0:
                    return_data_final.append(ex_return_data_final[i])
                elif len(return_data_final) > 0:
                    for j in range(len(return_data_final)):
                        if int(return_data_final[j][5]) == int(ex_return_data_final[i][5]):
                            return_data_final[j][1] = int(return_data_final[j][1]) + int(ex_return_data_final[i][1])
                            return_data_final[j][2] = int(return_data_final[j][2]) + int(ex_return_data_final[i][2])
                            return_data_final[j][3] = int(return_data_final[j][3]) + int(ex_return_data_final[i][3])
                            break
                        elif j+1 == len(return_data_final):
                            return_data_final.append(ex_return_data_final[i])
                            break
            for i in range(len(return_data_final)):
                if return_data_final[i][1] == 0:
                    return_data_final[i].append("0%")
                else:
                    return_data_final[i].append(str(round(return_data_final[i][3]/return_data_final[i][1]*100, 2))+"%")

            if export_data_flag:
                df = pd.DataFrame(return_data, columns=["Tên camera", "SL vào", "SL có KT", "SL không KT",
                                                        "Phút", "Giờ", "Ngày", "Tháng", "Năm"])
                file_name_export = "Dữ liệu thống kê " + str(camera_name_input) + "-" + str(day_input) + "-" + \
                                   str(month_input) + "-" + str(year_input) + "-" + ".csv"
                df.to_csv('./export_data/' + file_name_export)
            else:
                x = ["%d" % i for i in range(1, 25, 1)]
                y_in = [0 for i in range(1, 25, 1)]
                y_mask = [0 for i in range(1, 25, 1)]
                y_no_mask = [0 for i in range(1, 25, 1)]
                for elem in return_data:
                    for i in range(len(y_in)):
                        if elem[5] - 1 == i:
                            y_in[i] = y_in[i] + elem[1]
                            y_mask[i] = y_mask[i] + elem[2]
                            y_no_mask[i] = y_no_mask[i] + elem[3]
                plt.figure(figsize=(10, 5))
                plt.plot(x, y_in, label="Tổng số người vào")
                plt.plot(x, y_mask, label="Tổng số người có khẩu trang")
                plt.plot(x, y_no_mask, label="Tổng số người không khẩu trang")
                plt.title("Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                          + str(day_input) + "-"
                          + str(month_input) + "-"
                          + str(year_input) + " "
                          + "tại " + str(camera_name_input))
                plt.xlabel('Giờ trong ngày')
                plt.ylabel('Số lượng trong ngày')
                plt.legend()
                for i in range(len(y_in)):
                    if y_in[i] != 0:
                        plt.text(i - 0.2, y_in[i], str(y_in[i]), color=color, size='large')
                    if y_mask[i] != 0:
                        plt.text(i - 0.2, y_mask[i], str(y_mask[i]), color=color, size='large')
                    if y_no_mask[i] != 0:
                        plt.text(i - 0.2, y_no_mask[i], str(y_no_mask[i]), color=color, size='large')
                if not save_figure_flag:
                    if os.path.exists("./figure/" + name_of_figure):
                        os.remove("./figure/" + name_of_figure)
                    plt.savefig("./figure/" + name_of_figure)
                    plt.close()
                else:
                    plt.savefig("./figure/" + "Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                                + str(day_input) + "-"
                                + str(month_input) + "-"
                                + str(year_input) + " "
                                + "tại " + str(camera_name_input))
                    plt.close()
                # get sum of person, mask, no_mask
                sum_nguoi = sum(y_in)
                sum_co_KT = sum(y_mask)
                sum_khong_KT = sum(y_no_mask)
            time.sleep(0.5)
        elif statistics_type == "Thống kê theo Tháng":
            year_input = date[-4:]
            month_input = date[0:2]

            query = f"SELECT camera_name,num_in,num_mask,num_no_mask,minute,hour,day,month,year " \
                    f"FROM DATA WHERE camera_id = '{camera_id}' and year = {year_input} and month = {month_input} "
            c.execute(query)
            return_data = c.fetchall()
            # combine data and calculate percent
            ex_return_data_final = [list(ele) for ele in return_data]
            for i in range(len(ex_return_data_final)):
                ex_return_data_final[i][4] = "x"
                ex_return_data_final[i][5] = "x"
                if len(return_data_final) == 0:
                    return_data_final.append(ex_return_data_final[i])
                elif len(return_data_final) > 0:
                    for j in range(len(return_data_final)):
                        if int(return_data_final[j][6]) == int(ex_return_data_final[i][6]):
                            return_data_final[j][1] = int(return_data_final[j][1]) + int(ex_return_data_final[i][1])
                            return_data_final[j][2] = int(return_data_final[j][2]) + int(ex_return_data_final[i][2])
                            return_data_final[j][3] = int(return_data_final[j][3]) + int(ex_return_data_final[i][3])
                            break
                        elif j + 1 == len(return_data_final):
                            return_data_final.append(ex_return_data_final[i])
                            break
            for i in range(len(return_data_final)):
                if return_data_final[i][1] == 0:
                    return_data_final[i].append("0%")
                else:
                    return_data_final[i].append(
                        str(round(return_data_final[i][3] / return_data_final[i][1] * 100, 2)) + "%")

            # combine data and calculate percent
            ex_return_data_final = [list(ele) for ele in return_data]
            for i in range(len(ex_return_data_final)):
                ex_return_data_final[i][4] = "x"
                ex_return_data_final[i][5] = "x"

            if export_data_flag:
                df = pd.DataFrame(return_data, columns=["Tên camera", "SL vào", "SL có KT", "SL không KT",
                                                        "Phút", "Giờ", "Ngày", "Tháng", "Năm"])
                file_name_export = "Dữ liệu thống kê " + str(camera_name_input) + "-" + "-" + str(month_input) + "-" \
                                   + str(year_input) + "-" + ".csv"
                df.to_csv('./export_data/' + file_name_export)
            else:
                x = ["%d" % i for i in range(1, 32, 1)]
                y_in = [0 for i in range(1, 32, 1)]
                y_mask = [0 for i in range(1, 32, 1)]
                y_no_mask = [0 for i in range(1, 32, 1)]
                for elem in return_data:
                    for i in range(len(y_in)):
                        if elem[6] - 1 == i:
                            y_in[i] = y_in[i] + elem[1]
                            y_mask[i] = y_mask[i] + elem[2]
                            y_no_mask[i] = y_no_mask[i] + elem[3]
                plt.figure(figsize=(10, 5))
                plt.plot(x, y_in, label="Tổng số người vào")
                plt.plot(x, y_mask, label="Tổng số người có khẩu trang")
                plt.plot(x, y_no_mask, label="Tổng số người không khẩu trang")
                plt.title("Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                          + str(month_input) + "-"
                          + str(year_input) + " "
                          + "tại " + str(camera_name_input))
                plt.xlabel('Ngày trong tháng')
                plt.ylabel('Số lượng trong tháng')
                plt.legend()
                for i in range(len(y_in)):
                    if y_in[i] != 0:
                        plt.text(i - 0.3, y_in[i], str(y_in[i]), color=color, size='large')
                    if y_mask[i] != 0:
                        plt.text(i - 0.3, y_mask[i], str(y_mask[i]), color=color, size='large')
                    if y_no_mask[i] != 0:
                        plt.text(i - 0.3, y_no_mask[i], str(y_no_mask[i]), color=color, size='large')
                if not save_figure_flag:
                    if os.path.exists("./figure/" + name_of_figure):
                        os.remove("./figure/" + name_of_figure)
                    plt.savefig("./figure/" + name_of_figure)
                    plt.close()
                else:
                    plt.savefig("./figure/" + "Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                                + str(month_input) + "-"
                                + str(year_input) + " "
                                + "tại " + str(camera_name_input))
                    plt.close()
                # get sum of person, mask, no_mask
                sum_nguoi = sum(y_in)
                sum_co_KT = sum(y_mask)
                sum_khong_KT = sum(y_no_mask)
            time.sleep(0.5)
        elif statistics_type == "Thống kê theo Năm":
            year_input = date
            query = f"SELECT camera_name,num_in,num_mask,num_no_mask,minute,hour,day,month,year " \
                    f"FROM DATA WHERE camera_id = '{camera_id}' and year = {year_input}"
            c.execute(query)
            return_data = c.fetchall()
            # combine data and calculate percent
            ex_return_data_final = [list(ele) for ele in return_data]
            for i in range(len(ex_return_data_final)):
                ex_return_data_final[i][4] = "x"
                ex_return_data_final[i][5] = "x"
                ex_return_data_final[i][6] = "x"
                if len(return_data_final) == 0:
                    return_data_final.append(ex_return_data_final[i])
                elif len(return_data_final) > 0:
                    for j in range(len(return_data_final)):
                        if int(return_data_final[j][7]) == int(ex_return_data_final[i][7]):
                            return_data_final[j][1] = int(return_data_final[j][1]) + int(ex_return_data_final[i][1])
                            return_data_final[j][2] = int(return_data_final[j][2]) + int(ex_return_data_final[i][2])
                            return_data_final[j][3] = int(return_data_final[j][3]) + int(ex_return_data_final[i][3])
                            break
                        elif j + 1 == len(return_data_final):
                            return_data_final.append(ex_return_data_final[i])
                            break
            for i in range(len(return_data_final)):
                if return_data_final[i][1] == 0:
                    return_data_final[i].append("0%")
                else:
                    return_data_final[i].append(
                        str(round(return_data_final[i][3] / return_data_final[i][1] * 100, 2)) + "%")

            # combine data and calculate percent
            ex_return_data_final = [list(ele) for ele in return_data]
            for i in range(len(ex_return_data_final)):
                ex_return_data_final[i][4] = "x"
                ex_return_data_final[i][5] = "x"
                ex_return_data_final[i][6] = "x"

            if export_data_flag:
                df = pd.DataFrame(return_data, columns=["Tên camera", "SL vào", "SL có KT", "SL không KT",
                                                        "Phút", "Giờ", "Ngày", "Tháng", "Năm"])
                file_name_export = "Dữ liệu thống kê " + str(camera_name_input) + "-" + str(year_input) \
                                   + "-" + ".csv"
                df.to_csv('./export_data/' + file_name_export)
            else:
                x = ["%d" % i for i in range(1, 13, 1)]
                y_in = [0 for i in range(1, 13, 1)]
                y_mask = [0 for i in range(1, 13, 1)]
                y_no_mask = [0 for i in range(1, 13, 1)]
                for elem in return_data:
                    for i in range(len(y_in)):
                        if elem[7] - 1 == i:
                            y_in[i] = y_in[i] + elem[1]
                            y_mask[i] = y_mask[i] + elem[2]
                            y_no_mask[i] = y_no_mask[i] + elem[3]
                plt.figure(figsize=(10, 5))
                plt.plot(x, y_in, label="Tổng số người vào")
                plt.plot(x, y_mask, label="Tổng số người có khẩu trang")
                plt.plot(x, y_no_mask, label="Tổng số người không khẩu trang")
                plt.title("Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                          + str(year_input) + " "
                          + "tại " + str(camera_name_input))
                plt.xlabel('Tháng trong năm')
                plt.ylabel('Số lượng trong năm')
                plt.legend()
                for i in range(len(y_in)):
                    if y_in[i] != 0:
                        plt.text(i - 0.3, y_in[i], str(y_in[i]), color=color, size='large')
                    if y_mask[i] != 0:
                        plt.text(i - 0.3, y_mask[i], str(y_mask[i]), color=color, size='large')
                    if y_no_mask[i] != 0:
                        plt.text(i - 0.3, y_no_mask[i], str(y_no_mask[i]), color=color, size='large')
                if not save_figure_flag:
                    if os.path.exists("./figure/" + name_of_figure):
                        os.remove("./figure/" + name_of_figure)
                    plt.savefig("./figure/" + name_of_figure)
                    plt.close()
                else:
                    plt.savefig("./figure/" + "Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                                + str(year_input) + " "
                                + "tại " + str(camera_name_input))
                    plt.close()
                # get sum of person, mask, no_mask
                sum_nguoi = sum(y_in)
                sum_co_KT = sum(y_mask)
                sum_khong_KT = sum(y_no_mask)
                time.sleep(0.5)
        # commit database
        conn.commit()

    return return_data_final, sum_nguoi, sum_co_KT, sum_khong_KT


def plot_save_export_multiple_cameras(name_of_figure,
                                      color,
                                      camera_name_input,
                                      statistics_type,
                                      date,
                                      save_figure_flag,
                                      export_data_flag):
    # connect to sql database
    conn = sqlite3.connect('./database/final_data_base.db')
    c = conn.cursor()
    # variable
    return_data_final = []
    camera_id_list = []
    return_data_final_all_cameras = []
    sum_nguoi = 0
    sum_co_KT = 0
    sum_khong_KT = 0
    # get camera_id of camera_name_input
    if camera_name_input == "All cameras":
        query_id = "SELECT camera_id FROM DATA"
        # print("check all cameras")
    else:
        query_id = f"SELECT camera_id FROM DATA WHERE camera_name = '{camera_name_input}'"
        # print("check one camera")

    c.execute(query_id)
    id_data = c.fetchall()

    if len(id_data) == 0:
        app_warning_function.query_camera_id_warning()
    else:
        for i in range(len(id_data)):
            if int(id_data[i][0]) not in camera_id_list:
                camera_id_list.append(int(id_data[i][0]))
        # print("list of camera id: ", camera_id_list)
        if statistics_type == "Thống kê theo Ngày":
            year_input = date[-4:]
            day_input = date[0:2]
            month_input = date[3:5]

            query = f"SELECT camera_name,num_in,num_mask,num_no_mask,minute,hour,day,month,year " \
                    f"FROM DATA WHERE year = {year_input} and day = {day_input} " \
                    f"and month = {month_input} AND camera_id IN (%s)" % (', '.join(str(id) for id in camera_id_list))
            c.execute(query)
            return_data = c.fetchall()

            if export_data_flag:
                df = pd.DataFrame(return_data, columns=["Tên camera", "SL vào", "SL có KT", "SL không KT",
                                                        "Phút", "Giờ", "Ngày", "Tháng", "Năm"])
                file_name_export = "Dữ liệu thống kê " + str(camera_name_input) + "-" + str(day_input) + "-" + \
                                   str(month_input) + "-" + str(year_input) + "-" + ".csv"
                df.to_csv('./export_data/' + file_name_export)
            else:
                x = ["%d" % i for i in range(0, 24, 1)]
                y_in = [0 for i in range(0, 24, 1)]
                y_mask = [0 for i in range(0, 24, 1)]
                y_no_mask = [0 for i in range(0, 24, 1)]
                for elem in return_data:
                    for i in range(len(y_in)):
                        if elem[5] == i:
                            y_in[i] = y_in[i] + elem[1]
                            y_mask[i] = y_mask[i] + elem[2]
                            y_no_mask[i] = y_no_mask[i] + elem[3]
                plt.figure(figsize=(10, 5))
                plt.plot(x, y_in, label="Tổng số người vào")
                plt.plot(x, y_mask, label="Tổng số người có khẩu trang")
                plt.plot(x, y_no_mask, label="Tổng số người không khẩu trang")

                minimum_ele = min(y_in+y_mask+y_no_mask)
                maximum_ele = max(y_in+y_mask+y_no_mask)
                new_list = range(math.floor(minimum_ele), math.ceil(maximum_ele) + 1)
                if maximum_ele < 10:
                    plt.yticks(new_list)

                plt.title("Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                          + str(day_input) + "-"
                          + str(month_input) + "-"
                          + str(year_input) + " "
                          + "tại " + str(camera_name_input))
                plt.xlabel('Giờ trong ngày')
                plt.ylabel('Số lượng trong ngày')
                plt.legend()
                for i in range(len(y_in)):
                    if y_in[i] != 0:
                        plt.text(i - 0.2, y_in[i], str(y_in[i]), color=color, size='large')
                    if y_mask[i] != 0:
                        plt.text(i - 0.2, y_mask[i], str(y_mask[i]), color=color, size='large')
                    if y_no_mask[i] != 0:
                        plt.text(i - 0.2, y_no_mask[i], str(y_no_mask[i]), color=color, size='large')
                if not save_figure_flag:
                    if os.path.exists("./figure/" + name_of_figure):
                        os.remove("./figure/" + name_of_figure)
                    plt.savefig("./figure/" + name_of_figure)
                    plt.close()
                else:
                    plt.savefig("./figure/" + "Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                                + str(day_input) + "-"
                                + str(month_input) + "-"
                                + str(year_input) + " "
                                + "tại " + str(camera_name_input))
                    plt.close()
                # get sum of person, mask, no_mask
                sum_nguoi = sum(y_in)
                sum_co_KT = sum(y_mask)
                sum_khong_KT = sum(y_no_mask)

                camera_name_list = []
                for i in range(len(return_data)):
                    if return_data[i][0] not in camera_name_list:
                        camera_name_list.append(return_data[i][0])

                split_data = [[] for i in range(len(camera_name_list))]

                for j in range(len(split_data)):
                    for i in range(len(return_data)):
                        if str(return_data[i][0]) == camera_name_list[j]:
                            split_data[j].append(return_data[i])

                return_data_final = [[] for i in range(len(split_data))]

                for k in range(len(split_data)):
                    ex_return_data_final = [list(ele) for ele in split_data[k]]
                    for i in range(len(ex_return_data_final)):
                        ex_return_data_final[i][4] = "x"
                        if len(return_data_final[k]) == 0:
                            return_data_final[k].append(ex_return_data_final[i])
                        elif len(return_data_final[k]) > 0:
                            for j in range(len(return_data_final[k])):
                                if int(return_data_final[k][j][5]) == int(ex_return_data_final[i][5]):
                                    return_data_final[k][j][1] = int(return_data_final[k][j][1]) + int(ex_return_data_final[i][1])
                                    return_data_final[k][j][2] = int(return_data_final[k][j][2]) + int(ex_return_data_final[i][2])
                                    return_data_final[k][j][3] = int(return_data_final[k][j][3]) + int(ex_return_data_final[i][3])
                                    break
                                elif j+1 == len(return_data_final[k]):
                                    return_data_final[k].append(ex_return_data_final[i])
                                    break
                    for i in range(len(return_data_final[k])):
                        if return_data_final[k][i][1] == 0:
                            return_data_final[k][i].append("0%")
                        else:
                            return_data_final[k][i].append(str(round(return_data_final[k][i][3]/return_data_final[k][i][1]*100, 2))+"%")

                # flat return_data_final list
                return_data_final_all_cameras = [item for sublist in return_data_final for item in sublist]
                for i in range(len(return_data_final_all_cameras)):
                    return_data_final_all_cameras[i].insert(4, str(str(return_data_final_all_cameras[i][5])+"h " +
                                                                   str(return_data_final_all_cameras[i][6])+"/" +
                                                                   str(return_data_final_all_cameras[i][7])+"/" +
                                                                   str(return_data_final_all_cameras[i][8])))
                    for j in range(5):
                        return_data_final_all_cameras[i].pop(5)
                # print("return_data_final_all_cameras: ", return_data_final_all_cameras)
            time.sleep(0.5)
        elif statistics_type == "Thống kê theo Tháng":
            year_input = date[-4:]
            month_input = date[0:2]

            query = f"SELECT camera_name,num_in,num_mask,num_no_mask,minute,hour,day,month,year " \
                    f"FROM DATA WHERE year = {year_input} and month = {month_input} " \
                    f"AND camera_id IN (%s)" % (', '.join(str(id) for id in camera_id_list))
            c.execute(query)
            return_data = c.fetchall()

            if export_data_flag:
                df = pd.DataFrame(return_data, columns=["Tên camera", "SL vào", "SL có KT", "SL không KT",
                                                        "Phút", "Giờ", "Ngày", "Tháng", "Năm"])
                file_name_export = "Dữ liệu thống kê " + str(camera_name_input) + "-" + "-" + str(month_input) + "-" \
                                   + str(year_input) + "-" + ".csv"
                df.to_csv('./export_data/' + file_name_export)
            else:
                x = ["%d" % i for i in range(1, 32, 1)]
                y_in = [0 for i in range(1, 32, 1)]
                y_mask = [0 for i in range(1, 32, 1)]
                y_no_mask = [0 for i in range(1, 32, 1)]
                for elem in return_data:
                    for i in range(len(y_in)):
                        if elem[6] - 1 == i:
                            y_in[i] = y_in[i] + elem[1]
                            y_mask[i] = y_mask[i] + elem[2]
                            y_no_mask[i] = y_no_mask[i] + elem[3]
                plt.figure(figsize=(10, 5))
                plt.plot(x, y_in, label="Tổng số người vào")
                plt.plot(x, y_mask, label="Tổng số người có khẩu trang")
                plt.plot(x, y_no_mask, label="Tổng số người không khẩu trang")
                minimum_ele = min(y_in+y_mask+y_no_mask)
                maximum_ele = max(y_in+y_mask+y_no_mask)
                new_list = range(math.floor(minimum_ele), math.ceil(maximum_ele) + 1)
                if maximum_ele < 10:
                    plt.yticks(new_list)
                plt.title("Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                          + str(month_input) + "-"
                          + str(year_input) + " "
                          + "tại " + str(camera_name_input))
                plt.xlabel('Ngày trong tháng')
                plt.ylabel('Số lượng trong tháng')
                plt.legend()
                for i in range(len(y_in)):
                    if y_in[i] != 0:
                        plt.text(i - 0.3, y_in[i], str(y_in[i]), color=color, size='large')
                    if y_mask[i] != 0:
                        plt.text(i - 0.3, y_mask[i], str(y_mask[i]), color=color, size='large')
                    if y_no_mask[i] != 0:
                        plt.text(i - 0.3, y_no_mask[i], str(y_no_mask[i]), color=color, size='large')
                if not save_figure_flag:
                    if os.path.exists("./figure/" + name_of_figure):
                        os.remove("./figure/" + name_of_figure)
                    plt.savefig("./figure/" + name_of_figure)
                    plt.close()
                else:
                    plt.savefig("./figure/" + "Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                                + str(month_input) + "-"
                                + str(year_input) + " "
                                + "tại " + str(camera_name_input))
                    plt.close()
                # get sum of person, mask, no_mask
                sum_nguoi = sum(y_in)
                sum_co_KT = sum(y_mask)
                sum_khong_KT = sum(y_no_mask)

                camera_name_list = []
                for i in range(len(return_data)):
                    if return_data[i][0] not in camera_name_list:
                        camera_name_list.append(return_data[i][0])

                split_data = [[] for i in range(len(camera_name_list))]

                for j in range(len(split_data)):
                    for i in range(len(return_data)):
                        if str(return_data[i][0]) == camera_name_list[j]:
                            split_data[j].append(return_data[i])

                return_data_final = [[] for i in range(len(split_data))]

                for k in range(len(split_data)):
                    ex_return_data_final = [list(ele) for ele in split_data[k]]
                    for i in range(len(ex_return_data_final)):
                        ex_return_data_final[i][4] = "x"
                        ex_return_data_final[i][5] = "x"
                        if len(return_data_final[k]) == 0:
                            return_data_final[k].append(ex_return_data_final[i])
                        elif len(return_data_final[k]) > 0:
                            for j in range(len(return_data_final[k])):
                                if int(return_data_final[k][j][6]) == int(ex_return_data_final[i][6]):
                                    return_data_final[k][j][1] = int(return_data_final[k][j][1]) + int(
                                        ex_return_data_final[i][1])
                                    return_data_final[k][j][2] = int(return_data_final[k][j][2]) + int(
                                        ex_return_data_final[i][2])
                                    return_data_final[k][j][3] = int(return_data_final[k][j][3]) + int(
                                        ex_return_data_final[i][3])
                                    break
                                elif j + 1 == len(return_data_final[k]):
                                    return_data_final[k].append(ex_return_data_final[i])
                                    break
                    for i in range(len(return_data_final[k])):
                        if return_data_final[k][i][1] == 0:
                            return_data_final[k][i].append("0%")
                        else:
                            return_data_final[k][i].append(
                                str(round(return_data_final[k][i][3] / return_data_final[k][i][1] * 100, 2)) + "%")

                # flat return_data_final list
                return_data_final_all_cameras = [item for sublist in return_data_final for item in sublist]
                for i in range(len(return_data_final_all_cameras)):
                    return_data_final_all_cameras[i].insert(4, str(str(return_data_final_all_cameras[i][6])+"/" +
                                                                   str(return_data_final_all_cameras[i][7])+"/" +
                                                                   str(return_data_final_all_cameras[i][8])))
                    for j in range(5):
                        return_data_final_all_cameras[i].pop(5)
                # print("return_data_final_all_cameras: ", return_data_final_all_cameras)
            time.sleep(0.5)
        elif statistics_type == "Thống kê theo Năm":
            year_input = date
            query = f"SELECT camera_name,num_in,num_mask,num_no_mask,minute,hour,day,month,year " \
                    f"FROM DATA WHERE year = {year_input} " \
                    f"AND camera_id IN (%s)" % (', '.join(str(id) for id in camera_id_list))
            c.execute(query)
            return_data = c.fetchall()

            if export_data_flag:
                df = pd.DataFrame(return_data, columns=["Tên camera", "SL vào", "SL có KT", "SL không KT",
                                                        "Phút", "Giờ", "Ngày", "Tháng", "Năm"])
                file_name_export = "Dữ liệu thống kê " + str(camera_name_input) + "-" + str(year_input) \
                                   + "-" + ".csv"
                df.to_csv('./export_data/' + file_name_export)
            else:
                x = ["%d" % i for i in range(1, 13, 1)]
                y_in = [0 for i in range(1, 13, 1)]
                y_mask = [0 for i in range(1, 13, 1)]
                y_no_mask = [0 for i in range(1, 13, 1)]
                for elem in return_data:
                    for i in range(len(y_in)):
                        if elem[7] - 1 == i:
                            y_in[i] = y_in[i] + elem[1]
                            y_mask[i] = y_mask[i] + elem[2]
                            y_no_mask[i] = y_no_mask[i] + elem[3]
                plt.figure(figsize=(10, 5))
                plt.plot(x, y_in, label="Tổng số người vào")
                plt.plot(x, y_mask, label="Tổng số người có khẩu trang")
                plt.plot(x, y_no_mask, label="Tổng số người không khẩu trang")
                plt.title("Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                          + str(year_input) + " "
                          + "tại " + str(camera_name_input))
                minimum_ele = min(y_in+y_mask+y_no_mask)
                maximum_ele = max(y_in+y_mask+y_no_mask)
                new_list = range(math.floor(minimum_ele), math.ceil(maximum_ele) + 1)
                if maximum_ele < 10:
                    plt.yticks(new_list)
                plt.xlabel('Tháng trong năm')
                plt.ylabel('Số lượng trong năm')
                plt.legend()
                for i in range(len(y_in)):
                    if y_in[i] != 0:
                        plt.text(i - 0.3, y_in[i], str(y_in[i]), color=color, size='large')
                    if y_mask[i] != 0:
                        plt.text(i - 0.3, y_mask[i], str(y_mask[i]), color=color, size='large')
                    if y_no_mask[i] != 0:
                        plt.text(i - 0.3, y_no_mask[i], str(y_no_mask[i]), color=color, size='large')
                if not save_figure_flag:
                    if os.path.exists("./figure/" + name_of_figure):
                        os.remove("./figure/" + name_of_figure)
                    plt.savefig("./figure/" + name_of_figure)
                    plt.close()
                else:
                    plt.savefig("./figure/" + "Biểu đồ thống kê tổng số lượng người vào, có KT, không KT "
                                + str(year_input) + " "
                                + "tại " + str(camera_name_input))
                    plt.close()
                # get sum of person, mask, no_mask
                sum_nguoi = sum(y_in)
                sum_co_KT = sum(y_mask)
                sum_khong_KT = sum(y_no_mask)

                camera_name_list = []
                for i in range(len(return_data)):
                    if return_data[i][0] not in camera_name_list:
                        camera_name_list.append(return_data[i][0])

                split_data = [[] for i in range(len(camera_name_list))]

                for j in range(len(split_data)):
                    for i in range(len(return_data)):
                        if str(return_data[i][0]) == camera_name_list[j]:
                            split_data[j].append(return_data[i])

                return_data_final = [[] for i in range(len(split_data))]

                for k in range(len(split_data)):
                    ex_return_data_final = [list(ele) for ele in split_data[k]]
                    for i in range(len(ex_return_data_final)):
                        ex_return_data_final[i][4] = "x"
                        ex_return_data_final[i][5] = "x"
                        ex_return_data_final[i][6] = "x"
                        if len(return_data_final[k]) == 0:
                            return_data_final[k].append(ex_return_data_final[i])
                        elif len(return_data_final[k]) > 0:
                            for j in range(len(return_data_final[k])):
                                if int(return_data_final[k][j][7]) == int(ex_return_data_final[i][7]):
                                    return_data_final[k][j][1] = int(return_data_final[k][j][1]) + int(
                                        ex_return_data_final[i][1])
                                    return_data_final[k][j][2] = int(return_data_final[k][j][2]) + int(
                                        ex_return_data_final[i][2])
                                    return_data_final[k][j][3] = int(return_data_final[k][j][3]) + int(
                                        ex_return_data_final[i][3])
                                    break
                                elif j + 1 == len(return_data_final[k]):
                                    return_data_final[k].append(ex_return_data_final[i])
                                    break
                    for i in range(len(return_data_final[k])):
                        if return_data_final[k][i][1] == 0:
                            return_data_final[k][i].append("0%")
                        else:
                            return_data_final[k][i].append(
                                str(round(return_data_final[k][i][3] / return_data_final[k][i][1] * 100, 2)) + "%")

                # flat return_data_final list
                return_data_final_all_cameras = [item for sublist in return_data_final for item in sublist]
                for i in range(len(return_data_final_all_cameras)):
                    return_data_final_all_cameras[i].insert(4, str(str(return_data_final_all_cameras[i][7])+"/" +
                                                                   str(return_data_final_all_cameras[i][8])))
                    for j in range(5):
                        return_data_final_all_cameras[i].pop(5)
                # print("return_data_final_all_cameras: ", return_data_final_all_cameras)
                time.sleep(0.5)
        # commit database
        conn.commit()

    return return_data_final_all_cameras, sum_nguoi, sum_co_KT, sum_khong_KT
