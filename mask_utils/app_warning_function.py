from PyQt5 import QtCore, QtGui, QtWidgets
import os


def create_folders_to_save():
    if not os.path.exists("./Out"):
        os.mkdir("./Out")
        os.mkdir("./Out/Data")
        os.mkdir("./Out/Faces")
        os.mkdir("./Out/Videos")
    elif not os.path.exists("./Out/Data"):
        os.mkdir("./Out/Data")
    elif not os.path.exists("./Out/Faces"):
        os.mkdir("./Out/Faces")
    elif not os.path.exists("./Out/Videos"):
        os.mkdir("./Out/Videos")


# kiểm tra config file có hay không
def camera_config_flie():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không tìm thấy tập tin thiết đặt cho các camera, làm ơn kiểm tra lại!')
    alert.exec_()


# kiểm tra địa chỉ cho ip camera có đúng hay không khi chọn loại camera là IP Camera
def check_path_for_ip_camera():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Sai địa chỉ cho IP Camera, làm ơn kiểm tra và nhập lại lại!')
    alert.exec_()


# kiểm tra địa chỉ cho Webcam có đúng hay không khi chọn loại camera là Webcam
def check_path_for_webcam():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Sai địa chỉ cho Webcam, làm ơn kiểm tra và nhập lại lại!')
    alert.exec_()


# kiểm tra camera id khi tạo mới một camera
def check_new_camera_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Quá trình đăng kí thông tin cho Camera hiện tại chưa thành công, làm ơn kiểm tra lại!')
    alert.exec_()


# kiểm tra thông in các kiểm trong quá trình vẽ counting line:
def check_new_counting_lines():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Số lượng các điểm cần để tạo Vạch kiểm đếm đang không chính xác, làm ơn vẽ lại vạch kiểm đếm lại!')
    alert.exec_()


# kiểm tra thông tin camera trong config file
def check_camera_in_config_file():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Hiện tại camera này chưa được đăng kí và thêm vào tập tin thiết đặt cho các Cameras, làm ơn kiểm tra lại!')
    alert.exec_()


# kiểm tra tên mới của camera trước khi thực hiện rename
def check_camera_name_for_rename():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Làm ơn nhập tên mới cho camera nếu muốn thực hiện đổi tên cho camera này!')
    alert.exec_()


# kiểm tra alarm option
def check_alarm_option():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Làm ơn chọn phương thức cảnh báo áp dụng cho camera hiện tại!')
    alert.exec_()


# kiểm tra nhập password cho quá trình đổi pass
def check_password_input():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText("Làm ơn nhập đúng Mật khẩu cũ và thực hiện nhập Mật khẩu mới!")
    alert.exec_()


# kiểm old password cho quá trình đổi pass
def check_password_old():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText("Mật khẩu cũ chưa chính xác, làm ơn kiểm tra và nhập lại!")
    alert.exec_()


# kiểm old password cho quá trình đổi pass
def check_lenght_password_new():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText("Mật khẩu mới cần có ít nhất 5 kí tự, làm ơn nhập lại Mật khẩu phù hợp!")
    alert.exec_()


# Đổi password thành công
def check_password_changed_succesfully():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText("Mật khẩu đã được đổi thành công, xin cảm ơn!")
    alert.exec_()


# kiểm tra new password and confirm password
def check_password_new_and_confirm_new():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText("Mật khẩu mới và xác nhận Mật khẩu mới hiện không đồng nhất, làm ơn kiểm tra và nhập lại!")
    alert.exec_()


def check_config_file():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không tìm thấy tập tin thiết đặt cho các camera, làm ơn kiểm tra lại!')
    alert.exec_()


def check_camera_name():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Làm ơn nhập tên và chọn loại Camera cho bước đăng kí Camera này!')
    alert.exec_()


def check_camera_name_for_delete():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể thực hiện thao tác này do không có thông tin về Camera cần xóa, vui lòng chọn Camera cần '
                  'xóa!')
    alert.exec_()


def plotting_no_data_warning():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Hiện tại không có thông tin cho thời gian và Camera đã được nhập, làm ơn kiểm tra lại!')
    alert.exec_()


def query_camera_id_warning():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Hiện tại Camera này chưa được đăng kí hoặc mất trường thông tin Camera Id, làm ơn kiểm tra lại!')
    alert.exec_()


def stop_all_thread():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thay đổi về cấu hình camera(hoặc thêm mới camera) đã được ghi nhận, toàn bộ tiến trình đang '
                  'chạy(nếu có) sẽ bị dừng để đảm bảo thông tin, vui lòng kích hoạt lại!')
    alert.exec_()


# Báo nhập mật khẩu để lock hoặc unlock
def input_pass_for_lock():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Làm ơn nhập Mật khẩu trước khi thực hiện thao tác tiếp theo!')
    alert.exec_()


# Nhập sai mật khấu cho lock hoặc unlock
def input_pass_for_lock():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Mật khẩu chưa chính xác, xin vui lòng nhập lại!')
    alert.exec_()


# Địa điểm đã được đăng kí object id và không thể đăng kí lại
def check_object_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Địa điểm này đã được đăng kí Mã định danh, vui lòng không thực hiện lại thao tác này!')
    alert.exec_()


# Nhập tên thiết bị cho quá trình đăng kí Mã định danh
def check_name_for_object_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Vui lòng nhập tên của thiết bị cho quá trình đăng kí Mã định danh!')
    alert.exec_()


# Nhập licence cho quá trình đăng kí Mã định danh
def check_licence_for_object_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Vui lòng nhập Mã cấp phép cho quá trình đăng kí Mã định danh!')
    alert.exec_()


# Đăng kí Mã định danh thành công
def register_object_id_successful():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Mã định danh đã được đăng kí và cập nhật thành công!')
    alert.exec_()


# Đăng kí Mã định danh thất bại
def register_object_id_falied():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Đăng kí Mã định danh thất bại do sự cố ngắt kết nối mạng hoặc một vài lí do khác, vui lòng kiểm tra '
                  'đường truyền và thực hiện đăng kí lại!')
    alert.exec_()


# Đăng kí Mã định danh thất bại
def licence_already_used():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Đăng kí Mã định danh thất bại do Mã cấp phép đã được sử dụng hoặc không có sẵn, vui lòng kiểm tra '
                  'lại Mã cấp phép và thực hiện đăng kí lại!')
    alert.exec_()


# Object_id chưa được đăng kí
def non_object_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thiết bị này hiện chưa được đăng kí Mã định danh, vui lòng thực hiện bước đăng kí Mã định danh!')
    alert.exec_()


# Đăng kí camera_id thất bại
def register_camera_id_falied():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Đăng kí Mã định danh cho Camera thất bại do sự cố ngắt kết nối mạng hoặc một vài lí do khác, vui lòng kiểm tra '
                  'đường truyền và thực hiện đăng kí lại!')
    alert.exec_()


# Nhập địa chỉ truy cập cho camera
def check_camera_address():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Vui lòng nhập địa chỉ truy cập cho Camera hiện tại!')
    alert.exec_()


# Rename cho camera thất bại
def camera_rename_failed():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thực hiện đổi tên cho Camera hiện tại thất bại do sự cố ngắt kết nối mạng hoặc một vài lí do khác, vui lòng kiểm tra '
                  'đường truyền và thực hiện lại!')
    alert.exec_()


# Rename cho camera thất bại
def camera_deleted_or_not_registered():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Camera này đã bị xóa khỏi danh sách hoặc chưa từng được đăng kí, vui lòng kiểm tra lại!')
    alert.exec_()


def camera_delete_failed():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thực hiện xóa Camera hiện tại thất bại do sự cố ngắt kết nối mạng hoặc một vài lí do khác, vui lòng kiểm tra '
                  'đường truyền và thực hiện lại!')
    alert.exec_()


def camera_edit_draw_region():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể xác định địa chỉ của camera này, làm ơn thực hiện thao tác tìm kiếm để lấy địa chỉ '
                  'của camera này!')
    alert.exec_()


def no_camera_address_for_drawing():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể xác minh địa chỉ camera để thực hiện thao tác này, vui lòng xác minh lại hoặc nhập '
                  'địa chỉ cho camera!')
    alert.exec_()


def rename_camera_done():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Đổi tên cho camera thành công!')
    alert.exec_()


def search_for_camera_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể xác định Camera id, làm ơn nhập đúng tên Camera và thực hiện thao tác tìm kiếm!')
    alert.exec_()


def draw_counting_warning():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể thực hiện tao tác này khi chưa thực hiện vẽ vùng quan sát, làm ơn thực hiện tao tác vẽ '
                  'vùng quan sát!')
    alert.exec_()


def camera_has_not_add():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể thực hiện tao tác này khi Camera chưa được đăng kí, vui lòng thực hiện thao tác đăng kí '
                  'Camera!')
    alert.exec_()


def none_camera_in_config_file():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Hiện tại chưa có Camera nào được đăng kí và thêm vào danh sách, vui lòng thực hiện tao tác này!')
    alert.exec_()


def process_has_not_started():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Tiến trình chưa được khởi động, vui lòng không thực hiện thao tác này!')
    alert.exec_()


def camera_edit_cancel():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Tất cả thông tin đã được chỉnh sửa của Camera này sẽ bị xóa và sẽ không được lưu lại cho Camera này!')
    alert.exec_()


def camera_edit_cancel_not_yet():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thao tác chỉnh sửa thông tin của Camera chưa được thực hiện!')
    alert.exec_()


def cannot_connect_to_camera():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể kết nối tới Camera với địa chỉ hiện có, vui lòng xác minh lại địa chỉ Camera!')
    alert.exec_()


def camera_add_cancel_not_yet():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Hiện tại không có Camera nào đang được thêm vào danh sách, vui lòng không thực hiện thao tác này!')
    alert.exec_()


def camera_add_not_been_regiser():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Hiện tại không có Camera này chưa được đăng kí và sẽ được xóa khỏi thao tác này!')
    alert.exec_()


def can_not_cancel_register_object_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thiết bị này đã được đăng kí Mã định danh thành công, không thể thực hiện thao tác hủy bỏ này!')
    alert.exec_()


def cancel_register_object_id():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Các thông tin liên qua cho quá trình Đăng ký Mã định danh đã được xóa!')
    alert.exec_()


def cancel_password_changing():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Thao tác thay đổi Mật khẩu đã bị hủy!')
    alert.exec_()


# kiểm tra refresh
def check_refresh():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể thực hiện thao tác tạo mới Camera do chưa kiểm tra kết nối với Server, vui lòng '
                  'thực hiện thao tác này!')
    alert.exec_()


# kiểm tra refresh
def used_camera_name():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Không thể thực hiện thao tác tạo mới Camera do tên Camera đã được sử dụng, vui lòng nhập tên khác '
                  'cho Camera!')
    alert.exec_()


def refresh():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Mọi thứ đã được làm mới và kiểm tra kết nối, vui lòng không thực hiện lại thao tác này!')
    alert.exec_()


def no_alarm_option_selected():
    alert = QtWidgets.QMessageBox()
    alert.setWindowTitle("Cảnh báo")
    alert.setText('Cấu hình cảnh báo không được chọn, thông tin cấu hình sẽ được thiết đặt mặc định chế độ '
                  'cảnh báo Âm Thanh!')
    alert.exec_()