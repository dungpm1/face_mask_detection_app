# Face Mask Detection Application
Ứng dụng phát hiện đối tượng không mang khẩu trang
## Description
This Windows APP is using for supervised application. It can run with multiple cameras. Beside Face Mass detection function, it also includes some orther functions such as Cameras Management, Report and Statistics.
## Install the packages
### With GPU option
pip install -r requirements.txt
### With CPU option
pip install -r requirements_cpu.txt
pip install torch==1.7.0+cpu torchvision==0.8.1+cpu torchaudio===0.7.0 -f https://download.pytorch.org/whl/torch_stable.html
## Usage
### Create new(blank) database and reset configs file
python create_dataBase_configs_file.py
### Run the APP
python GG-Face-Mask-Application.py
